package chiu.kevin.basicbroadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // 2.Create receiver obj and intent filter obj
    private val mNCR = NetworkChangeReceiver()
    private val mIntentFilter = IntentFilter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // 3.Add action into intent filter obj
        mIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        // 4.Register the receiver dynamically, some need to add permission in the manifest.
        registerReceiver(mNCR, mIntentFilter)

        btn_send_broadcast.setOnClickListener {
            val intent = Intent("hello.hello.hello")
            sendBroadcast(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // 5.Remember to unregister the registered receiver
        unregisterReceiver(mNCR)
    }

    // 1.Create receiver class
    class NetworkChangeReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            context?.let {
                val connectionManager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectionManager.activeNetworkInfo
                // networkInfo should be determined whether it is null first. Otherwise networkInfo.isAvailable will crash.
                if (networkInfo != null && networkInfo.isAvailable) {
                    Toast.makeText(context, "Network is working.", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(context, "No Network,", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
