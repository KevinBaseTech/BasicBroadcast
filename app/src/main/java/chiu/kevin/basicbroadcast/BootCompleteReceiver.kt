package chiu.kevin.basicbroadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.widget.Toast

/**
 * Created by Kobe on 2017/11/16.
 * Static register receiver
 */
class BootCompleteReceiver : BroadcastReceiver() {
    val BC = "android.intent.action.BOOT_COMPLETED"
    override fun onReceive(context: Context?, intent: Intent?) {
        if (BC == intent?.action) {
            context?.let {
                Toast.makeText(it, "System boot completed.", Toast.LENGTH_LONG).show()
                val i = Intent(it, MainActivity::class.java)
                // Need to set up flags =
                i.flags = FLAG_ACTIVITY_NEW_TASK
                it.startActivity(i)
            }
        }
    }
}