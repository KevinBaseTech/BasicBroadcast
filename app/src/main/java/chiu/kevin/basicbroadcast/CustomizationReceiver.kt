package chiu.kevin.basicbroadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

/**
 * Created by Kobe on 2017/11/16.
 * Customization receiver
 */
class CustomizationReceiver : BroadcastReceiver() {
    val BC = "hello.hello.hello"
    override fun onReceive(context: Context?, intent: Intent?) {
        if (BC == intent?.action) {
            context.let {
                Toast.makeText(it, "Customization broadcast get", Toast.LENGTH_LONG).show()
            }
        }
    }
}